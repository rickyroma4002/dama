public class Casella {
    enum coordinataX{
        A, B, C, D, E, F, G, H
    }
    enum coordinataY{
        _1, _2, _3, _4, _5, _6, _7, _8
    }
    private coordinataX coordX;
    private coordinataY coordY;
    private String colore;
    public boolean isEmpty=true;

    public Casella(coordinataY coordY, coordinataX coordX, String colore){
        this.coordY=coordY;
        this.coordX=coordX;
        this.colore=colore;
    }

    public coordinataX getCoordX() {
        return coordX;
    }

    public coordinataY getCoordY() {
        return coordY;
    }
    public String getColore(){
        return colore;
    }
    public String toString(){
        return "O";
    }
}


