public class Pedina {
    protected String colore;
    protected boolean isMagneda;
    protected Casella position;

    public Pedina(Casella position, String colore){
        this.colore=colore;
        this.position=position;
    }
    public String toString(){
        return "-";
    }
    public void mossa(Casella newPosition){
        if (newPosition.isEmpty && newPosition.getColore().equals("black")) {
            this.position = newPosition;
        }
    }
    public void mossaMangia(Casella newPosition){
        if (newPosition.isEmpty && newPosition.getColore().equals("black")) {
            this.position = newPosition;
        }
    }
}
